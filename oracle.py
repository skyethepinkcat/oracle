import orb
import sys
import config

# Options

name = "Skye"

Greeting = "DAWN OF A NEW DAY\nHere's what's going on today, %s:\n" % name

tbreak = " \n-----------------------------------\n"

rainTxt = "It's going to rain today!"


def temperatureText(side, temp):
    return "The " + side + " temperature is " + str(int(temp)) + " degrees."
eventText = "The following is on your calendar:"

end = "\nThat's it for today!"

def main():
    weather = orb.zeus()
    weather.refresh(config.apikey)

    if (weather.getRaining()): ## Add the rain text if it's raining today
        crainTxt = tbreak + rainTxt
    else:
        crainTxt = ""
    ceventText = "" ## Current event text
    for i in config.calendars:
        cal = orb.calendar(i["url"],i["user"],i["password"]) ## Create calendar object
        events = cal.getToday(i["user"],i["password"]) ## Get the events of today
        if (events != []): ## If events aren't empty
            ceventText += tbreak + eventText + "\n" ## Initial event text
            for i in events:
                if (i != ""):
                    ceventText += "\n-"+i ## Add each event

    message = Greeting + crainTxt + tbreak + temperatureText("max",weather.getMaxTemp()) + tbreak + temperatureText("min",weather.getMinTemp()) + ceventText + tbreak + end ## Put message together
    print(message)

main()
